import connection
import json
import datetime

from datetime import date
from flask import Flask, render_template, url_for, redirect, request

app = Flask(__name__)

@app.route('/')
def index():
    connection.create_and_connnect()
    return render_template('index.html')


@app.route('/maestros')
def lista_maestros():
    data_maestros = connection.ver_maestros()
    return render_template('lista_maestros.html', data=data_maestros)


@app.route('/grados/')
@app.route('/grados/<error>')
def lista_grados(error=None):
    data_grados = connection.ver_grados()
    data_maestros = connection.ver_maestros()
    return render_template('lista_grados.html', data=data_grados, dataP=data_maestros, error = error)


@app.route('/alumnos')
def lista_alumnos():
    datos_alumnos = connection.ver_alumnos()
    data_grados = connection.ver_grados() 
    datos_asignacion = connection.buscar_asignacion()
    prueba = []
    for ids in datos_asignacion:
        prueba.append(ids.id)
    return render_template('lista_alumnos.html', data=datos_alumnos, dataG=data_grados, dataA = prueba)


@app.route('/agregar-grado')
def agregar_grado(): 
    try:
        dataG = json.loads(request.cookies.get('dataG'))
    except TypeError:
        data_maestros = connection.ver_maestros()
        return render_template('agregar_grado.html', data=data_maestros)
    else:
        if dataG.get('grado'):
            nuevo_grado = connection.actualizar_grado(grado=dataG.get('grado'), nombre=dataG.get('nombre'), profesorId=dataG.get('profesorId'))
        else:
            nuevo_grado = connection.agregar_grado(nombre=dataG.get('nombre'), profesorId=dataG.get('profesorId'))
        if nuevo_grado:
            response = redirect(url_for('lista_grados'))
        else:
            response = redirect(url_for('lista_grados', error = "El Maestro ya tiene un grado asignado!"))
        response.delete_cookie('dataG', '/')
        return response
    


@app.route('/agregar-maestro')
def agregar_maestro():
    try:
        data = json.loads(request.cookies.get('data'))
    except TypeError:
        return render_template('agregar_maestro.html')
    else:
        if data.get('maestro'):
            connection.actualizar_maestro(maestro=data.get('maestro'), nombre=data.get('nombre'), apellido=data.get('apellido'), genero=data.get('genero'))
        else:
            connection.agregar_maestro(nombre=data.get('nombre'), apellido=data.get('apellido'), genero=data.get('genero'))
            # flash('Bien!')
        response = redirect(url_for('lista_maestros'))
        response.delete_cookie('data', '/')
        return response


@app.route('/agregar-alumno')
def agregar_alumno():
    try:
        dataA = json.loads(request.cookies.get('dataA'))
    except TypeError:
        return render_template('agregar_alumno.html')
    else:
        if dataA.get('alumno'):
            connection.actualizar_alumno(alumno=dataA.get('alumno'), nombre=dataA.get('nombre'), apellidos=dataA.get('apellidos'), genero=dataA.get('genero'), fecha_nacimiento=date(int(dataA.get('anio')),int(dataA.get('mes')),int(dataA.get('dia'))))
        else:
            connection.agregar_alumno(nombre=dataA.get('nombre'), apellidos=dataA.get('apellidos'), genero=dataA.get('genero'), fecha_nacimiento=date(int(dataA.get('anio')),int(dataA.get('mes')),int(dataA.get('dia'))))
        response = redirect(url_for('lista_alumnos'))
        response.delete_cookie('dataA', '/')
        return response


@app.route('/agregar-asignacion')
def agregar_asignacion():
    dataAs = json.loads(request.cookies.get('dataAs'))
    if dataAs.get('asignacion'):
        connection.actualizar_alumno_grado(asignacion=dataAs.get('asignacion'), alumnoId=dataAs.get('alumno'), gradoId=dataAs.get('grado'), seccion=dataAs.get('seccion'))
    else:
        connection.agregar_alumno_grado(alumnoId=dataAs.get('alumno'), gradoId=dataAs.get('grado'), seccion=dataAs.get('seccion'))
    response = redirect(url_for('lista_alumnos'))
    response.delete_cookie('dataAs', '/')
    return response
        

@app.route("/resultado-grado", methods=['POST'])
def resultado_grado():
    response = redirect(url_for('agregar_grado'))
    response.set_cookie('dataG', json.dumps(dict(request.form.items())))
    return response


@app.route("/resultado-maestro", methods=['POST'])
def resultado_maestro():
    response = redirect(url_for('agregar_maestro'))
    response.set_cookie('data', json.dumps(dict(request.form.items())))
    return response


@app.route("/resultado-alumno", methods=['POST'])
def resultado_alumno():
    response = redirect(url_for('agregar_alumno'))
    response.set_cookie('dataA', json.dumps(dict(request.form.items())))
    return response


@app.route("/resultado-asignar", methods=['POST'])
def resultado_asignar():
    response = redirect(url_for('agregar_asignacion'))
    response.set_cookie('dataAs', json.dumps(dict(request.form.items())))
    return response



@app.route("/eliminar-maestro/<id_maestro>")
def eliminar_maestro(id_maestro):
    connection.eliminar_maestro(id_maestro)
    response = redirect(url_for('lista_maestros'))
    return response


@app.route("/eliminar-grado/<id_grado>")
def eliminar_grado(id_grado):
    connection.eliminar_grado(id_grado)
    response = redirect(url_for('lista_grados'))
    return response


@app.route("/eliminar-alumno/<id_alumno>")
def eliminar_alumno(id_alumno):
    connection.eliminar_alumno(id_alumno)
    response = redirect(url_for('lista_alumnos'))
    return response


@app.route("/eliminar-asignacion/<id_asignacion>")
def eliminar_asignacion(id_asignacion):
    connection.eliminar_asignacion(id_asignacion)
    response = redirect(url_for('lista_alumnos'))
    return response


if __name__ == '__main__':
    app.run(debug=True)


