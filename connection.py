from peewee import *


db = SqliteDatabase('colegio.db')

class Maestro(Model):
    nombre = TextField()
    apellido = TextField()
    genero = TextField()

    class Meta:
        database = db


class Grado(Model):
    nombre = TextField()
    profesorId = ForeignKeyField(Maestro, related_name='grado', unique=True)

    class Meta:
        database = db


class Alumno(Model):
    nombre=TextField()
    apellidos=TextField()
    genero=TextField()
    fecha_nacimiento= DateField()

    class Meta:
        database = db
    

class AlumnoGrado(Model):
    alumnoId=ForeignKeyField(Alumno, related_name='alumnoGrado')
    gradoId=ForeignKeyField(Grado, related_name='alumnoGrado')
    seccion= TextField()

    class Meta:
        database = db


def create_and_connnect():
    '''Connnect to DB and Create Tables'''
    db.connect()
    db.create_tables([Maestro,Grado, Alumno, AlumnoGrado])


def agregar_maestro(**data):
    nuevo_maestro = Maestro.create(nombre=data.get('nombre'), apellido=data.get('apellido'), genero=data.get('genero'))
    if nuevo_maestro:
        return "Se ha creado el maestro"
    else:
        return "Ha ocurrido un error"


def agregar_grado(**dataG):
    try:
        maestro = Maestro.get_by_id(dataG.get('profesorId'))
        nuevo_grado = Grado.create(nombre=dataG.get('nombre'), profesorId=maestro)
    except IntegrityError:
        return False
    else:
        return nuevo_grado
    


def agregar_alumno(**data):
    nuevo_alumno = Alumno.create(nombre=data.get('nombre'), apellidos=data.get('apellidos'), genero=data.get('genero'), fecha_nacimiento=data.get('fecha_nacimiento') )
    if nuevo_alumno:
        return "Se ha creado el alumno"
    else:
        return "Ha ocurrido un error"


def agregar_alumno_grado(**data):
    alumno = Alumno.get_by_id(data.get('alumnoId'))
    grado = Grado.get_by_id(data.get('gradoId'))
    nuevo_registro = AlumnoGrado.create(alumnoId=alumno, gradoId=grado, seccion=data.get('seccion'))
    if nuevo_registro:
        return "Se ha creado el registro"
    else:
        return "Ha ocurrido un error"


def buscar_asignacion():
    query = AlumnoGrado.select()
    return query


def ver_maestros():
    maestros = Maestro.select()
    return maestros


def ver_grados():
    grados = Grado.select()
    return grados


def ver_alumnos():
    alumnos = Alumno.select()
    return alumnos


def actualizar_alumno_grado(**data):
    asignacion = AlumnoGrado.get_by_id(data.get('asignacion'))
    asignacion.alumnoId = data.get('alumnoId')
    asignacion.gradoId = data.get('gradoId')
    asignacion.seccion = data.get('seccion')
    asignacion.save()
    return "Asignacion actualizada"


def actualizar_maestro(**data):
    maestro = Maestro.get_by_id(data.get('maestro'))
    maestro.nombre = data.get('nombre')
    maestro.apellido = data.get('apellido')
    maestro.genero = data.get('genero')
    maestro.save()
    return "Maestro actualizado"


def actualizar_alumno(**data):
    alumno = Alumno.get_by_id(data.get('alumno'))
    alumno.nombre = data.get('nombre')
    alumno.apellidos = data.get('apellidos')
    alumno.genero = data.get('genero')
    alumno.fecha_nacimiento = data.get('fecha_nacimiento')
    alumno.save()
    return "Maestro actualizado"


def actualizar_grado(**data):
    maestro = Maestro.get_by_id(data.get('profesorId'))
    grado = Grado.get_by_id(data.get('grado'))
    grado.nombre = data.get('nombre')
    grado.profesorId = maestro
    grado.save()
    return "Maestro actualizado"


def eliminar_maestro(maestro):
    query = Maestro.get_by_id(maestro)
    query.delete_instance(recursive=True)
    return "Maestro eliminado"


def eliminar_grado(grado):
    query = Grado.get_by_id(grado)
    query.delete_instance(recursive=True)
    return "Grado eliminado"


def eliminar_alumno(alumno):
    query = Alumno.get_by_id(alumno)
    query.delete_instance(recursive=True)
    return "Alumno eliminado"

def eliminar_asignacion(asignacion):
    query = AlumnoGrado.get_by_id(asignacion)
    query.delete_instance()
    return "Asignacion eliminada"